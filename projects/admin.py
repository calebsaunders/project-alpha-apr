from django.contrib import admin
from .models import Project

# Register your models here.


class ProjectsAdmin(admin.ModelAdmin):

    list_display = ("name", "description", "owner")
    list_filter = ("owner",)
    search_fields = ("name", "description")


admin.site.register(Project)
