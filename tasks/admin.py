from django.contrib import admin
from .models import Task


# Register your models here.
class TasksAdmin(admin.ModelAdmin):
    default_auto_field = "django.db.models.BigAutoField"
    name = "task"


admin.site.register(Task)
